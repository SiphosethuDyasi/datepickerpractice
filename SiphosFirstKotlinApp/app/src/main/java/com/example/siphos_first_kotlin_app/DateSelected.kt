package com.example.siphos_first_kotlin_app

interface DateSelected {
    fun receiveDate(year:Int, month:Int, dayOfMonth:Int)
}