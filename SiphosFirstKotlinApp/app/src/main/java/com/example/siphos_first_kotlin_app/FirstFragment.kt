package com.example.siphos_first_kotlin_app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.siphos_first_kotlin_app.databinding.FragmentFirstBinding
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment(), DateSelected{

    private var _binding: FragmentFirstBinding? = null
    private var but : Button? = null
    private var text : TextView? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById<Button>(R.id.toast_button).setOnClickListener{
            val myToast = Toast.makeText(context, "Hello Toast!", Toast.LENGTH_SHORT);
            myToast.show();
        }

        view.findViewById<Button>(R.id.count_button).setOnClickListener{
            countMe(view);
        }

        but = view.findViewById(R.id.btnOpenDialog)
        text = view.findViewById(R.id.textview_first)

        view.findViewById<Button>(R.id.btnOpenDialog).setOnClickListener{

            val newDate = MyDialog(this)
            newDate.show(childFragmentManager, "date picker")
           // findNavController().navigate(R.id.action_FirstFragment_to_myDialog)
        }

    }

    private fun countMe(view: View){
        val showCountTextView = view.findViewById<TextView>(R.id.textview_first);
        val countString = showCountTextView.text.toString();
        var count = countString.toInt();
        count++;

        showCountTextView.text = count.toString();
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun receiveDate(year: Int, month: Int, dayOfMonth: Int) {
        val calendar = GregorianCalendar()
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.YEAR, year)

        val viewFormatter = SimpleDateFormat("MMM dd, yyyy")
        var viewFormattedDate : String = viewFormatter.format(calendar.time)
        text?.setText(viewFormattedDate)
    }
}